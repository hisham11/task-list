class TaskModel {
  TaskModel(this.taskName);

  final String taskName;
  bool isCompleted = false;

  void markTaskAsCompleted() {
    isCompleted = !isCompleted;
  }
}
