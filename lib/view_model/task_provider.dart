import 'package:get/get.dart';
import 'package:template_flutter/view/general_widget/dialog_widget.dart';
import 'package:template_flutter/view/tasks/add_task_form.dart';
import 'package:template_flutter/view_model/base_view_model.dart';

import '../model/task_model.dart';

class TaskProvider extends BaseViewModel {
  List<TaskModel> taskList = [];

  void addToTask(String? taskName) {
    if (taskName != null && taskName.isNotEmpty) {
      taskList.insert(0,TaskModel(taskName));
      Get.back();
    }
    notifyListeners();
  }

  showAddTaskForm() {
    return AppDialog.sampleDialog(
        dialogTitle: 'Add new Task', content: AddTaskForm());
  }

  void markTaskAsCompleted(int index) {
    taskList[index].markTaskAsCompleted();
    notifyListeners();
  }

  void deleteTask(int index) {
    if (taskList.isNotEmpty) {
      taskList.removeAt(index);
      Get.back();
    }

    notifyListeners();
  }

  String getTaskName(int index) {
    return taskList[index].taskName;
  }
}
