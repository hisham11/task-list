import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppDialog{

   static sampleDialog({String? buttonTitle,
      required String dialogTitle,Widget? content,
       String? buttonDescription, void Function()? buttonOnPressed}){
     return
       Get.defaultDialog(
         title: dialogTitle  ,content: content,middleText: '',
           textConfirm: buttonTitle,titleStyle: const TextStyle(fontSize: 20)
       );

  }

}

