import 'package:flutter/material.dart';
class CustomButton extends StatelessWidget {
  const CustomButton({super.key,required this.onPressed,required this.title});
  final  void Function() onPressed;
  final String title;
  @override
  Widget build(BuildContext context) {
    return
    Center(
      child: ElevatedButton(
        style:ElevatedButton.styleFrom(backgroundColor: Colors.blue,
            shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
        ),
        onPressed:onPressed,
        child:
         Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child:  Text(title,
            style: const TextStyle(color: Colors.white),)),),
    );
  }
}
