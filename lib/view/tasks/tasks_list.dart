
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter/view/general_widget/dialog_widget.dart';
import 'package:template_flutter/view/tasks/delete_task_dialog.dart';
import 'package:template_flutter/view_model/task_provider.dart';
class TaskList extends StatelessWidget {
  const TaskList({super.key});

  get checkedItems => null;

  @override
  Widget build(BuildContext context) {
    return
      Consumer<TaskProvider>(

            builder: (context,bloc,child){
              return
                Scaffold(
                  body: SizedBox(height: Get.height,
                    child: Column(mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child:
                              ListView.builder(
                                  itemCount: bloc.taskList.length,
                                  itemBuilder: (context,index){
                                    return

                                      Container(
                                        padding: const EdgeInsets.symmetric(vertical: 5),
                                        margin: const EdgeInsets.only(bottom: 2),
                                        color:bloc.taskList[index].isCompleted?Colors.green.withOpacity(0.2):
                                          Colors.grey.withOpacity(0.1),
                                        child: ListTile(
                                          title: Text(bloc.getTaskName(index),style:
                                          const TextStyle(fontWeight: FontWeight.bold),),
                                          leading: Checkbox(checkColor:
                                          Colors.white,activeColor: Colors.blue,
                                            value:bloc.taskList[index].isCompleted,
                                            onChanged: (value) {
                                              // Call a function to update the checked status
                                              bloc.markTaskAsCompleted(index);
                                            },
                                          ),
                                          trailing: IconButton(
                                            icon:  Icon(Icons.delete,color: Colors.grey.withOpacity(0.7),),
                                            onPressed: () {
                                              // Call a function to delete the item at the tapped task index
                                             AppDialog.sampleDialog(dialogTitle: 'Are you sure to delete task?',
                                             content: DeleteDialog(taskIndex: index));
                                            },
                                          ),

                                        ),
                                      );

                                  }),
                        ),
                      ],
                    ),
                  ),
                    floatingActionButton: FloatingActionButton(onPressed:
                    bloc.showAddTaskForm,
                      backgroundColor: Colors.blue,child: const Icon(Icons.add),),
                );
            }

      );

  }
}
