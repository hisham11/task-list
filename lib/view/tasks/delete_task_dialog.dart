import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../view_model/task_provider.dart';
import '../general_widget/button_widget.dart';
class DeleteDialog extends StatelessWidget {
  const DeleteDialog({super.key, required this.taskIndex});
  final int taskIndex;
  @override
  Widget build(BuildContext context) {
    return
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CustomButton(title: 'Yes',
              onPressed:(){
                Provider.of<TaskProvider>(context, listen: false).deleteTask(taskIndex);
              }
          ),
          CustomButton(onPressed:(){Get.back();}, title:'Cancel')
        ],
      );
  }
}
