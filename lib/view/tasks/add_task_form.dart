import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter/view_model/task_provider.dart';

import '../general_widget/button_widget.dart';

class AddTaskForm extends StatelessWidget {
  AddTaskForm({super.key});

 final TextEditingController taskNameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 10,),
        TextField(
          key:const Key('taskNameController'),
          autofocus: true,
          controller: taskNameController,maxLength: 50,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomButton(title: 'Save',
                onPressed:(){
                  Provider.of<TaskProvider>(context, listen: false).
                  addToTask(taskNameController.text);
                }
                  ),
                CustomButton(onPressed:(){Get.back();}, title:'Cancel')
              ],
            )
    ]
    );
  }

}
