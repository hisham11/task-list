import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import 'package:template_flutter/view/tasks/tasks_list.dart';
import 'package:template_flutter/view_model/task_provider.dart';
void main() {
  runApp(
       MultiProvider(providers:
        [
          ChangeNotifierProvider<TaskProvider>(create:(_)=> TaskProvider()),
        ],
        child:
      const MyApp()
   )) ;
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
       // useMaterial3: true,
      ),
      home:  const MyHomePage(title: 'Tasks List') ,)
    //  )
    ;

  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body:
        const TaskList(),

    );
  }
}
