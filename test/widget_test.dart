// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter/main.dart';
import 'package:template_flutter/view/tasks/add_task_form.dart';
import 'package:template_flutter/view_model/task_provider.dart';

void main() {


  testWidgets('Task List test', (WidgetTester tester) async {
    // Build our app and trigger a frame.

      await tester.pumpWidget(
      MultiProvider(providers:
      [
        ChangeNotifierProvider<TaskProvider>(create:(_)=> TaskProvider()),
      ],
          child: const MyApp()));

      // Verify FloatingActionButton.
      expect(find.byType(FloatingActionButton), findsOneWidget);
      // Verify there is no tasks.
      expect(find.byType(ListTile), findsNothing);

      // Tap the '+' icon and trigger a frame.
      await tester.tap(find.byIcon(Icons.add));
      await tester.pump();

      // Verify there is  add task dialog.
      expect(find.byType(AddTaskForm), findsOneWidget);

      /// add task name
      final founder = find.byKey(const Key('taskNameController'));
      // Enter task name
      await tester.enterText(founder, 'task1');
      // save task
      await tester.tap(find.text('Save'));
      await tester.pump();
      // expect found new task added to list
      expect(find.byType(ListTile), findsOneWidget);
      expect(find.text('task1'), findsOneWidget);

     /// check task as complete
      await tester.tap(find.byType(Checkbox));
      await tester.pump();
      final checkbox = tester.firstWidget<Checkbox>(find.byType(Checkbox));
      // check if checkbox checked
      expect(checkbox.value, true);
      await tester.pump();

      /// delete task
      final deleteFounder = find.byIcon(Icons.delete);
      expect(deleteFounder, findsOneWidget);
      await tester.tap(deleteFounder);
      await tester.pump();
      // check if delete task dialog diplayed
      await tester.tap(find.text('Yes'));
      await tester.pump();
      // check if task deleted
      expect(find.byType(ListTile), findsNothing);

    });

}
